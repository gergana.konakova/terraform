variable "project_id" {
  type        = string
  description = "The project ID to host the cluster in"
}

variable "region" {
  type        = string
  default     = "europe-west1"
  description = "The region to host the cluster in"
}

variable "zones" {
  type        = list(string)
  description = "The zones to host the cluster in"
  default     = []
}

variable "vpc" {
  type        = string
  description = "The VPC the cluster will be using"
}

variable "subnetwork" {
  type        = string
  description = "The subnetwork the cluster will be using"
}

variable "node_locations" {
  type        = string
  description = "The node locations"
}

variable "min_nodes" {
  type        = number
  description = "The minimum number of nodes in the nodepool"
}

variable "max_nodes" {
  type        = number
  description = "The maximum number of nodes in the nodepool"
}

variable "process_app_sa" {
  type        = string
  description = "The Service Account used to run the Cluster"
}

