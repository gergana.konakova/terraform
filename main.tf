module "gke" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  version = "27.0.0"

  project_id          = var.project_id
  name                = "my-process-app"
  region              = var.region
  zones               = var.zones
  network             = var.vpc
  subnetwork          = var.subnetwork
  http_load_balancing = false
  network_policy      = false
  node_pools = [
    {
      name               = "cpu-intense"
      machine_type       = "c2-standard-4"
      node_locations     = var.node_locations
      min_count          = var.min_nodes
      max_count          = var.max_nodes
      disk_size_gb       = 100
      disk_type          = "pd-ssd"
      image_type         = "COS_CONTAINERD"
      auto_repair        = true
      auto_upgrade       = true
      service_account    = var.process_app_sa
      preemptible        = true
      initial_node_count = 80
    },
  ]

  node_pools_oauth_scopes = {
    all = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}